CREATE DATABASE energy_data_collection_db;

USE energy_data_collection_db;
GRANT ALL ON *.* TO 'power4all'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE energy_data_db;

USE energy_data_db;
GRANT ALL ON *.* TO 'power4all'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE weather_service_db;

USE weather_service_db;
GRANT ALL ON *.* TO 'power4all'@'%';
FLUSH PRIVILEGES;