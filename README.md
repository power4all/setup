# Power4All setup

## Development environment setup

```
docker-compose up -d
```

### RabbitMQ

**management:** http://localhost:15672/

**username:** guest
**password:** guest



# Add new database

```mysql
CREATE DATABASE energy_data_db;

USE energy_data_db;
GRANT ALL ON *.* TO 'power4all'@'%';
FLUSH PRIVILEGES;
```

